import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/HomePage'
import TermsCondition from '@/pages/TermsCondition'
import NotFound from '@/pages/404'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/terms-conditions',
      name: 'TermsCondition',
      component: TermsCondition
    },
    {
      path: '*',
      name: 'Not Found',
      component: NotFound
    }
  ]
})
