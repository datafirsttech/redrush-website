import Vue from 'vue';
import Vuex, { Store } from "vuex";

Vue.use(Vuex)

export default new Store({
    state:{
        showStatic: true
    },
    mutations:{
        HIDE_STATIC(state){
            state.showStatic =false;
        }
    },
    actions:{
        disable_static({commit}){
            commit('HIDE_STATIC')
        }
    }
})